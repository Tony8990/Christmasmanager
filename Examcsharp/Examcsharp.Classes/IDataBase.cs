﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examcsharp.Classes
{
    public interface IDataBase
    {
        User GetUser(User user);
         
        bool UpdateOrder(Order order);
        
        bool UpdateToy(Toy toy);
        
        IEnumerable<Toy> GetAllToy();
        
        IEnumerable<Order> GetAllOrder();
        
        Toy GetToy(string id);

        Order GetOrder(string id);
    }
}