﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examcsharp.Classes
{
    public enum TypeStatus
    {
        Progress = 0,
        Ready_To_Send = 1,
        Done = 2
    }
}