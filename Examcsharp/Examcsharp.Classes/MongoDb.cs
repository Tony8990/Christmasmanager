﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading;
using MongoDB.Bson.Serialization;

namespace Examcsharp.Classes
{
    public class MongoDb : IDataBase
    {
        private IMongoDatabase database
        {
            get
            {
                return MongoConnection.Instance.Database;
            }
        }
        public User GetUser(User user)
        {
            IMongoCollection<User> userCollection =database.GetCollection<User>("users");
            return userCollection.Find(_ => _.Email== user.Email && _.Password == user.Password).FirstOrDefault();
        }

       
        public bool UpdateOrder(Order order)
        {
            IMongoCollection<Order> orderCollection = database.GetCollection<Order>("orders");
            var filter = Builders<Order>.Filter.Eq("_id", ObjectId.Parse(order.ID));
            var update = Builders<Order>.Update
                .Set("Status", order.Status);
                
                
            try
            {
                orderCollection.UpdateOne(filter, update);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateToy(Toy toy)
        {
            IMongoCollection<Toy> toysCollection = database.GetCollection<Toy>("toys");
            var filter = Builders<Toy>.Filter.Eq("_id", ObjectId.Parse(toy.ID));
            var update = Builders<Toy>.Update
                .Set("Name", toy.Name)
                .Set("Amount", toy.Amount);
                
                
                
            try
            {
                toysCollection.UpdateOne(filter, update);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        public IEnumerable<Toy> GetAllToy()
        {
            IMongoCollection<Toy> toysCollection = database.GetCollection<Toy>("toys");
            return toysCollection.Find(new BsonDocument()).ToList();
        }

        public IEnumerable<Order> GetAllOrder()
        {
            IMongoCollection<Order> orderCollection = database.GetCollection<Order>("orders");
           // IMongoCollection<Toy> toyCollection = database.GetCollection<Toy>("toys");
           // List<Order> ordersList =
            return orderCollection.Find(new BsonDocument()).SortBy(e => e.RequestDate).ToList();
            /*foreach (Order order in ordersList)
            {
                List<string> toyNames = order.Toys.Select(x => x.Name).ToList();
                order.Toys = toyCollection.Find(_ => toyNames.Contains(_.Name)).ToList();
            }
            return ordersList;*/

            /*List<Order> orders = orderCollection.Find(new BsonDocument()).SortBy(o => o.RequestDate).ToList();
            foreach (Order order in orders)
            {
                List<Toy> distinctToys =new List<Toy>();
                foreach (Toy originalToy in order.Toys)
                {
                    bool flag = true;
                    foreach (Toy toy in distinctToys)
                    {
                        if (toy.Name.Equals(originalToy.Name))
                        {
                            flag = false;
                            break;

                        }
                        
                    }
                    if (flag)
                            distinctToys.Add(originalToy);

                }
                order.Toys = distinctToys;

            }

            return orders;
*/
        }

        public Toy GetToy(string id)
        {
            IMongoCollection<Toy> toysCollection = database.GetCollection<Toy>("toys");
            
            return toysCollection.Find(_ => _.ID == id).FirstOrDefault();
        }

        public Order GetOrder(string id)
        {
            IMongoCollection<Order> ordersCollection = database.GetCollection<Order>("orders");
            
           return ordersCollection.Find(_ => _.ID == id).FirstOrDefault();
               
            

            
        }
    }
}