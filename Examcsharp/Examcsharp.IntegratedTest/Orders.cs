﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Examcsharp.Classes;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using ExamDB = Examcsharp.Classes.MongoDb;

namespace Examcsharp.IntegratedTest
{
    [TestClass]
    public class Orders
    {
        private IMongoDatabase db;
        
        private const string KID = "test-kid";
        private const TypeStatus STATUS = TypeStatus.Ready_To_Send; 
        private ObjectId orderId = ObjectId.GenerateNewId();


        [TestInitialize]
        public void Initialize()
        {
            MongoClientSettings settings = new MongoClientSettings();
            MongoClient client = new MongoClient(MongoDbConfig.ConnectionString);
            db = client.GetDatabase(MongoDbConfig.DBName);
            IMongoCollection<Order> collection = db.GetCollection<Order>("orders");
            collection.InsertOne(new Order
            {
                ID = orderId.ToString(),
                Status = STATUS,
                Kid= KID
               
            });
        }
        [TestCleanup]
        public void Cleanup()
        {
            if (db != null)
            {
                db.DropCollection("orders");
            }
        }
        [TestMethod]
        public void GetOrder_Should_Return_TestOrder()
        {
            var db = new ExamDB();
            var order = db.GetOrder(orderId.ToString());
            Assert.IsNotNull(order);
        }
        [TestMethod]
        public void GetAllOrder_Should_Return_TestOrder()
        {
            var db = new ExamDB();
            var list = db.GetAllOrder();
            Assert.AreEqual(1, list.Count());
        }
        [TestMethod]
        public void UpdateOrder_Should_Return_True()
        {
            var db = new ExamDB();
            var order = db.GetOrder(orderId.ToString());
            order.Status = TypeStatus.Progress;
            Assert.IsTrue(db.UpdateOrder(order));
        }
        
        
    }
}