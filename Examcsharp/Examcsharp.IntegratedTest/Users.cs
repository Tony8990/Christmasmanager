﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Examcsharp.Classes;
using System.Linq;
using MongoDB.Driver;
using ExamDB = Examcsharp.Classes.MongoDb;

namespace Examcsharp.IntegratedTest
{
    [TestClass]
    public class Users
    {
        private IMongoDatabase db;
        private const string SCREENNAME = "test-user";
        private const string EMAIL = "test-EMAIL";
        private const string PASSWORD= "test-password" ;
        private const bool ISADMIN= true ;
        
        [TestInitialize]
        public void Initialize()
        {
            MongoClientSettings settings = new MongoClientSettings();
            MongoClient client = new MongoClient(MongoDbConfig.ConnectionString);
            db = client.GetDatabase(MongoDbConfig.DBName);
            IMongoCollection<User> collection = db.GetCollection<User>("users");
            collection.InsertOne(new User
            {
                ScreenName = SCREENNAME,
                Email = EMAIL,
                Password = PASSWORD,
                isAdmin = ISADMIN
            });
        }
        
        [TestCleanup]
        public void Cleanup()
        {
            if (db != null)
            {
                db.DropCollection("toys");
            }
        }
        
        [TestMethod]
        public void GetUsers_Should_Return_TestUsers()
        {
            var db = new ExamDB();
            var test = new User
            {
                ScreenName = SCREENNAME,
                Email = EMAIL,
                Password = PASSWORD,
                isAdmin = ISADMIN
            };
            var user = db.GetUser(test);
            Assert.IsNotNull(user);
        }
        
    }
}