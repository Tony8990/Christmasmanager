﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Examcsharp.Classes;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using ExamDB = Examcsharp.Classes.MongoDb;
namespace Examcsharp.IntegratedTest
{
    [TestClass]
    public class Toys
    {
        private IMongoDatabase db;
        private const string TOY_NAME = "test-toys";
        private const int AMOUNT = 1;
        private const decimal COST = 15M ;
        private ObjectId toysId = ObjectId.GenerateNewId();


        [TestInitialize]
        public void Initialize()
        {
            MongoClientSettings settings = new MongoClientSettings();
            MongoClient client = new MongoClient(MongoDbConfig.ConnectionString);
            db = client.GetDatabase(MongoDbConfig.DBName);
            IMongoCollection<Toy> collection = db.GetCollection<Toy>("toys");
            collection.InsertOne(new Toy
            {
                ID = toysId.ToString(),
                Name = TOY_NAME,
                Amount = AMOUNT,
                Cost =COST
            });
        }
        [TestCleanup]
        public void Cleanup()
        {
            if (db != null)
            {
                db.DropCollection("toys");
            }
        }
        [TestMethod]
        public void GetToys_Should_Return_TestToys()
        {
            var db = new ExamDB();
            var toy = db.GetToy(toysId.ToString());
            Assert.IsNotNull(toy);
        }
        [TestMethod]
        public void GetAllToys_Should_Return_TestToys()
        {
            var db = new ExamDB();
            var list = db.GetAllToy();
            Assert.AreEqual(1, list.Count());
        }
    }
}
