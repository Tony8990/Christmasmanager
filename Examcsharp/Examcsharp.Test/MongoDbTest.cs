﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Examcsharp.Classes;
using Examcsharp.Test.Mocks;
using Moq;

namespace Examcsharp.Test
{
    [TestClass]
    public class MongoDbTest
    {
        [TestMethod]
        public void Get_Toy_Should_Throw_Exception_When_ID_Has_Value()
        {
            var mock = new Mock<IDataBase>();
            mock.Setup(toy => toy.GetToy(It.Is<string>(Id=>Id==string.Empty))).Throws<ArgumentException>();

            mock.Object.GetToy("test");
        }

        [TestMethod]
        public void Get_Order_Should_Throw_Exception_When_ID_Has_Value()
        {
            var mock = new Mock<IDataBase>();
            mock.Setup(order => order.GetOrder(It.Is<string>(Id => Id == string.Empty))).Throws<ArgumentException>();

            mock.Object.GetOrder("test");
        }

        [TestMethod]
        public void Get_User_Should_Throw_Exception_When_User_Has_Value()
        {
            var mock = new Mock<IDataBase>();
            User user = new User();

            mock.Setup( _=> _.GetUser(It.Is<User>(u => u == null))).Throws<NullReferenceException>();
            mock.Object.GetUser(user);
           
        }

        
        [TestMethod]
        public void Update_Order_Should_Throw_Exception_When_ID_Has_Value()
        {

            var mock = new Mock<IDataBase>();
            var order = new Order();
            mock.Setup(_ => _.UpdateOrder(It.Is<Order>(o => o == null))).Throws<NullReferenceException>();

            mock.Object.UpdateOrder(order);
        }
        [TestMethod]
        public void Get_All_Toy_Should_Throw_Exception_When_Value()
        {
            var mock = new Mock<IDataBase>();

            mock.Setup(_ => _.GetAllToy());

            mock.Object.GetAllToy();
        }
        [TestMethod]
        public void Get_All_Order_Should_Throw_Exception_When_Value()
        {
            var mock = new Mock<IDataBase>();

            mock.Setup(_ => _.GetAllOrder());

            mock.Object.GetAllOrder();
        }


    }
}
