﻿using Examcsharp.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Examcsharp.Models
{
    public class Users
    {
        public Users()
        {
            EntityList = new List<User>(); 
        }
        public List<User> EntityList { get; private set; }
    }
}