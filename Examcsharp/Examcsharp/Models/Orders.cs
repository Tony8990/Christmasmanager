﻿using Examcsharp.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Examcsharp.Models
{
    public class Orders
    {
        public Orders()
        {
            EntityList = new List<Order>();
            ToyList =new List<Toy>();
        }

        public List<Order> EntityList { get; private set; }


        public List<Toy> ToyList { get; private set;}


        public decimal TotCost = 0;
    }
}