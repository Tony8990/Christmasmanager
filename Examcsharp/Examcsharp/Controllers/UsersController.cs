﻿using System.Web.Mvc;
using Examcsharp.Classes;
using MongoDB.Bson;
using Examcsharp.Models;
using System;
using System.Linq;
using ExamMongoDb = Examcsharp.Classes.MongoDb;
using System.Text;
using System.Security.Cryptography;

namespace Examcsharp.Controllers
{
    public class UsersController : Controller
    {
        // GET
        public ActionResult Login()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Login(User user)
        {
            ExamMongoDb db = new ExamMongoDb();
            
            string Encrypt(string text)
            {
                byte[] data = Encoding.UTF8.GetBytes(text);
                byte[] resultHash;
                SHA512 shaM = new SHA512Managed();
                resultHash = shaM.ComputeHash(data);
                StringBuilder result = new StringBuilder();
                for (int i = 0; i < resultHash.Length; i++)
                {
                    result.Append(resultHash[i].ToString("X2").ToLower());
                }
                return result.ToString();
            }
            user.Password = Encrypt(user.Password);
            var usr = db.GetUser(user);
            
            if (usr!=null)
            {
                Session["ScreenName"] = usr.ScreenName.ToString();
                Session["IsAdmin"] = Convert.ToBoolean(usr.isAdmin.ToString());
                Session["ID"] = usr.ID.ToString();
                return RedirectToAction($"../Home");
            }
            else
            {
                ModelState.AddModelError("","Username or Password Error");
            }
            return View();
        }
        
        public ActionResult Logout()
        {
            if (Session["ID"]!=null)
            {
                Session.Clear();
                return RedirectToAction("Logout");
            }
            return View();
        }
    }
    
    

}