﻿using MongoDB.Bson;
using Examcsharp.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Examcsharp.Models;
using ExamMongoDb = Examcsharp.Classes.MongoDb;

namespace Examcsharp.Controllers
{
    public class OrdersController : Controller
    {
        // GET: Orders
        public ActionResult Index()
        {
            if (Session["IsAdmin"] != null)
            {
                ExamMongoDb db = new ExamMongoDb();
                var orders = db.GetAllOrder();
                var toys = db.GetAllToy();
                Orders model = new Orders();
                model.EntityList.AddRange(orders);
                model.ToyList.AddRange(toys.ToList());
                    
                
                return View(model);
            }
            else
            {
                return RedirectToAction($"../Users/Login");
            }

        }

        public ActionResult Details(string id)
        {
            if (Session["IsAdmin"] != null)
            {
                ExamMongoDb db = new ExamMongoDb();

                var orders = db.GetOrder(id);
                Order model = new Order();
                model.ID = orders.ID;
                model.RequestDate = orders.RequestDate;
                model.Kid = orders.Kid;
                model.Toys= orders.Toys;
                model.Status = orders.Status;
                
                return View(model);
               

            }
            else
            {
                return RedirectToAction($"../Users/Login");
            }


        }

        public ActionResult Edit(string id)
        {
            if (Session["IsAdmin"] != null)
            {
                ExamMongoDb db = new ExamMongoDb();
                ViewBag.TypeStatus = new TypeStatus();
                var order = db.GetOrder(id);
                Order model = new Order();
                model.ID = order.ID;
                model.Kid = order.Kid;
                model.Status = order.Status;
                model.RequestDate = order.RequestDate;
                model.Toys = order.Toys;
                return View(model);
            }
            else
            {
                return RedirectToAction($"../Users/Login");
            }
        }

        public ActionResult Save(string id, TypeStatus typeStatus)
        {

            if (Session["IsAdmin"] != null || typeStatus.Equals(TypeStatus.Done))
            {
                ExamMongoDb db = new ExamMongoDb();
                bool result;
                var order = db.GetOrder(id);
                var toys = db.GetAllToy().ToList();
                bool allToysIsPresent = ControlToy(order, toys);
                
                


                if ( allToysIsPresent == true || typeStatus.ToString() == "Progress")
                {
                    switch (order.Status.ToString())
                    {
                        case "Progress":
                            if (typeStatus.ToString() != "Progress")
                            {
                                RemoveToy(db, order, toys);
                                result = db.UpdateOrder(new Order
                                {
                                    ID = id,
                                    Status = typeStatus
                                });
                            }
                            break;
                        case "Ready-To-Send":
                            if (typeStatus.ToString() == "Progress")
                            {
                                AddToy(db, order, toys);
                                result = db.UpdateOrder(new Order
                                {
                                    ID = id,
                                    Status = typeStatus
                                });
                            }
                            else if (typeStatus == TypeStatus.Done)
                            {
                                if (typeStatus.ToString() != "Progress")
                                {
                                    RemoveToy(db, order, toys);
                                    result = db.UpdateOrder(new Order
                                    {
                                        ID = id,
                                        Status = typeStatus
                                    });
                                }
                            }
                            break;
                    }
                    return RedirectToAction("Index");
                }
                
                
              return  RedirectToAction($"../Toys/MissingToy");
                
                
                //return RedirectToAction("Index",new {result=result});
            }
            
            
            return RedirectToAction($"../Users/Login");
            
            
        }
   

        private bool ControlToy(Order order, List<Toy> toys)
        {
            foreach (var toyOrder in order.Toys)
            {
                foreach (var toy in toys)
                {
                    if (toy.Name == toyOrder.Name)
                    {
                        if (toy.Amount == 0)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
     
        private void RemoveToy(ExamMongoDb db, Order order, List<Toy> toys)
        {
            bool result;
            foreach (var toyOrder in order.Toys)
            {
                foreach (var toy in toys)
                {
                    if (toy.Name.Equals(toyOrder.Name))
                    {
                        result = db.UpdateToy(new Toy
                        {
                            ID = toy.ID,
                            Name = toy.Name,
                            Amount = toy.Amount - 1
                        });
                    }
                }
            }
        }
     
        private void AddToy(ExamMongoDb db, Order order, List<Toy> toys)
        {
            bool result;
            foreach (var toyOrder in order.Toys)
            {
                foreach (var toy in toys)
                {
                    if (toy.Name.Equals(toyOrder.Name))
                    {
                        result = db.UpdateToy(new Toy
                        {
                            ID = toy.ID,
                    
                            Name = toy.Name,
                            Amount = toy.Amount + 1
                        });
                    }
                }
            }
        }
     
        
    }
}