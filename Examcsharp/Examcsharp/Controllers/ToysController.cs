﻿using MongoDB.Bson;
using Examcsharp.Classes;
using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Examcsharp.Models;
using ExamMongoDb = Examcsharp.Classes.MongoDb;


namespace Examcsharp.Controllers
{
    public class ToysController : Controller
    {
        public ActionResult Index()
        {
            if (Session["IsAdmin"]!=null)
            {
                ExamMongoDb db = new ExamMongoDb();
                var toys = db.GetAllToy();
                Toys model = new Toys();
                model.EntityList.AddRange(toys.ToList());
                return View(model);   
            }
            else
            {
                return RedirectToAction($"../Users/Login");
            }
            
        }
        public ActionResult MissingToy()
        {
            if (Session["IsAdmin"] != null)
            {
                ExamMongoDb db = new ExamMongoDb();
                var toys = db.GetAllToy();
                Toys model = new Toys();
                model.EntityList.AddRange( toys.ToList());
                return View(model);
            }
            else
            {
                return RedirectToAction($"../Users/Login");
            }
        }
        
    }
}